<?php

namespace Lpdw\DesignPatterns\Adapter;

class PhpDiscours implements Discours
{
    private $texte;

    public function __construct(string $texte)
    {
        $this->texte = $texte;
    }

    public function lireTexte():string
    {
        return $this->texte;
    }
}
