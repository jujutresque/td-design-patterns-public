<?php

namespace Lpdw\DesignPatterns\Adapter;

class SpeakerApplication
{
    public function talk(Speech $speech):string
    {
        return $speech->readText();
    }
}
