<?php

namespace Lpdw\DesignPatterns\Iterator\Collection;

use Lpdw\DesignPatterns\Iterator\Model\Person;

class PersonCollection implements \Iterator
{
    private $currentIndex = 0;

    private $collection;

    public function __construct(array $collection)
    {
        $this->collection = $collection;
        $this->rewind();
    }

    public function current():?Person
    {
        return $this->collection[$this->currentIndex];
    }

    public function next()
    {
        $this->currentIndex++;
    }

    public function key():int
    {
        return $this->currentIndex;
    }

    public function valid():bool
    {
        if($this->collection[$this->currentIndex + 1] = null){
            return false;
        }
        else{
            return true;
        }
    }

    public function rewind()
    {
        $this->currentIndex = 0;
    }
}
