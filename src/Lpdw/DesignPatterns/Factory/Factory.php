<?php

namespace Lpdw\DesignPatterns\Factory;

abstract class Factory
{
    abstract public function create():Vehicle;
}
