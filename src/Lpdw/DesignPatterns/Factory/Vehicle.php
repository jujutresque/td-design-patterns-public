<?php

namespace Lpdw\DesignPatterns\Factory;

interface Vehicle
{
    public function countWheels():int;
}
