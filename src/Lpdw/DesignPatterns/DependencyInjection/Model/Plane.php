<?php

namespace Lpdw\DesignPatterns\DependencyInjection\Model;

class Plane implements Vehicle
{
    public function movingTo(string $address):string
    {
        return 'l\'avion a atteri à ' . $address;
    }
    public function travelTo(string $address):string
    {
        return 'l\'avion a atteri à ' . $address;
    }
}