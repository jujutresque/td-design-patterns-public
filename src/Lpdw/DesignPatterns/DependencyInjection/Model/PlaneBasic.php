<?php

namespace Lpdw\DesignPatterns\DependencyInjection\Model;

class PlaneBasic implements Vehicle
{
    public function movingTo(string $address):string
    {
        return 'l\'avion a atteri à ' . $address;
    }
}
