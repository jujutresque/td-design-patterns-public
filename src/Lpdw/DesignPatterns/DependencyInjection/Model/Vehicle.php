<?php

namespace Lpdw\DesignPatterns\DependencyInjection\Model;

interface Vehicle
{
    public function movingTo(string $address):string;
    public function travelTo(String $adresse):string;
}
