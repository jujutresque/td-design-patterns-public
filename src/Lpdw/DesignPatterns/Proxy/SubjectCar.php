<?php

namespace Lpdw\DesignPatterns\Proxy;

interface SubjectCar
{
    public function driveCar();
}
