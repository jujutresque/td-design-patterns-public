<?php

namespace Lpdw\DesignPatterns\Singleton;

class Logger
{
    private static $instance;

    public static function getInstance():Logger
    {
    }

    private function __construct()
    {
    }

    public function log(string $level, string $message)
    {
        // Code pour enregistrer le message du log.
    }
}
