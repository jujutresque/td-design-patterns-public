<?php

use SellingPrice\CalculationService;
use SellingPrice\PriceProvider\SupplierPriceFromLocalFile;
use SellingPrice\Storage\JsonFile;
require __DIR__ . '/vendor/autoload.php';

$SellingPriceCalculationService= new CalculationService(new SupplierPriceFromLocalFile());
$SellingPriceCalculationService->process(new JsonFile(__DIR__ . '/resources/selling-prices/'));