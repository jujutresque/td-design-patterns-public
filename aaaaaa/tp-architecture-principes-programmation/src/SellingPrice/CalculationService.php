<?php

namespace SellingPrice;
use SellingPrice\PriceProvider\SupplierPriceFromLocalFile;
use SellingPrice\Storage\JsonFile;

class CalculationService
{
    const PRODUCT_PROFIT_MARGIN = 1.15;

    private $supplierPriceFromLocalFile;

    public function __construct(SupplierPriceFromLocalFile $supplierPriceFromLocalFile) {
        $this->supplierPriceFromLocalFile = $supplierPriceFromLocalFile;
    }

    public function process()
    {
        $supplierPriceList = $this->supplierPriceFromLocalFile->retrieve();

        $productFinalPrice = $this->calculateSellingPrice($supplierPriceList);

        $jsonFile->save($productFinalPrice);
    }


    public function calculateSellingPrice($suppliersPricesList): array
    {
        $productFinalPrice = [];
        foreach ($suppliersPricesList as $productLabel => $prices) {
            $productFinalPrice[$productLabel] = number_format(array_sum($prices) / count($prices) * self::PRODUCT_PROFIT_MARGIN, 2);
        }
        return $productFinalPrice;
    }

}
