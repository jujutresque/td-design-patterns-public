<?php

namespace Lpdw\DesignPatterns\Proxy;

use PHPUnit\Framework\TestCase;

class ProxyCarTest extends TestCase
{

    /**
     * @test
     */
    public function driverCouldDriveCar()
    {
        $driver = new Driver(20);
        $car = new ProxyCar($driver);
        self::assertEquals($car->driveCar(),'Car has been driven!');
    }

    /**
     * @test
     */
    public function driverCouldNotDriveCar()
    {
        $driver = new Driver(15);
        $car = new ProxyCar($driver);
        self::expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage('Driver is too young!');
        $car->driveCar();
    }
}
