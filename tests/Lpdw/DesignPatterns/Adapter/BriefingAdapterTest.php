<?php

namespace Lpdw\DesignPatterns\Adapter;


use PHPUnit\Framework\TestCase;

class BriefingAdapterTest extends TestCase
{
    /**
     * @test
     */
    public function shouldAdapteBriefing()
    {
        $speakerApplication = new SpeakerApplication();

        $briefing = new PhpBriefing();
        $briefing->addInstruction('instruction 1');
        $briefing->addInstruction('instruction 2');

        $briefingAdapter = new BriefingAdapter($briefing);

        $this->assertThat($speakerApplication->talk($briefingAdapter), self::equalTo("instruction 1 instruction 2"));
    }
}
