<?php

namespace Lpdw\DesignPatterns\DependencyInjection\Service;

use PHPUnit\Framework\TestCase;

class TravelBasicServiceTest extends TestCase
{
    /**
     * @test
     */
    public function shouldtravelTo()
    {
        $travelService = new TravelBasicService();
        self::assertThat($travelService->travelTo('Paris'),
            self::equalTo('l\'avion a atteri à Paris'));
    }
}
